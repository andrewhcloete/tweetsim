------------------------------------------------------------------------------
# Wednesday, 04 July 2018
- o Updated REAME for release
- o BUG: Mashup of usernames
- o Abstracted `Repository` class make parseAscii a DRY method

------------------------------------------------------------------------------
# Tuesday, 03 July 2018
- o Added `minimist`package to parse cmd options
- o Create classes for User and User repositories
- o Create classes for Tweet and Tweet repositories
- o Create deserialiser routine for User and Tweet repositories
- o Implemented basic unit tests for Tweet and User repositories

------------------------------------------------------------------------------
# Monday, 02 July 2018
- o Set up basic typescript project structure
- o Add Jest unit testing framework