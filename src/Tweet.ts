/**
 * Author: Andrew Cloete
 * Created: 3 July 2018
 */

export class Tweet {

    constructor(public username: string, public msg: string) {

        if(username == '' || typeof(username) == undefined || typeof(msg) == undefined) {
            throw new Error(`Invalid tweet: username "${username}", msg "${msg}"`)
        }
        
    }
}