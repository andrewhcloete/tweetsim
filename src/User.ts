/**
 * Author: Andrew Cloete
 * Created: 3 July 2018
 */

export class User {
    name: string
    following: Set<string>

    constructor(name: string) {
        
        if(name == '' || typeof(name) == undefined ) {
            throw new Error(`Invalid username: username "${name}""`)
        }
        this.name = name;
        this.following = new Set();
        this.addFollowing(this.name);  // Every user automatically follow themselves
    }

    public addFollowing(followingName: string) {
        this.following.add(followingName);
    }

}