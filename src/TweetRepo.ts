/**
 * Author: Andrew Cloete
 * Created: 3 July 2018
 */

import { Tweet } from "./Tweet";
import { Repository, IAsciiExportLine } from "./Repo"

export class TweetRepository extends Repository {
    _tweets: Tweet[];
    private LINEBREAK = "\r\n" // Assumes windows style line endings (\r\n)
    private SEPARATOR = "> "

    constructor() {
        super();
        this._tweets = [];
    }

    /**
     * Returns a filtered by user copy of all the tweets
     * If an empty array is supplied all the tweets are returned
     * 
     * @param usernames 
     */
    public getTweets(usernames: string[]) {

        if (usernames.length == 0) {
            return this._tweets.slice() // Returns a copy of _tweets
        } 

        return this._tweets.filter(tweet => {
            return usernames.indexOf(tweet.username) >= 0; // Returns a subset copy of _tweets
        })
    }

    /**
     * Returns an array of all the unique users present in the tweets
     */
    public getUsers(): string[] {
        const userSet = new Set();
        this._tweets.forEach(tweet => {
            userSet.add(tweet.username);
        })

        return Array.from(userSet.values());
    }


    /**
     * Deserialises a 7-bit ASCII string to tweet objects
     * 
     * @param tweetAscii The single raw string of tweet messages
     */
    buildRepository(tweetAscii: string) {

        const entries = this.parseAscii(tweetAscii, this.LINEBREAK, this.SEPARATOR)

        entries.forEach(entry => {
            let tweet: Tweet;
            try {
                tweet  = new Tweet(entry.key, entry.value);
                this._tweets.push(tweet);
            }
            catch (e) {
                console.warn(e.message);
            }
        })
    }

}