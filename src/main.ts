/**
 * Author: Andrew Cloete
 * Created: 2 July 2018
 * 
 * Purpose: Main procedure that reads ASCII files into User and Tweet objects 
 * and prints a simulated twitter feed for each user based on their following
 * connections
 * 
 */


import { readFileSync } from "fs";
import { ICmdArgs, readAndValidateArgs } from "./utils";


import { UserRepository } from "./UserRepo";
import { TweetRepository } from "./TweetRepo";

// Parse and validate commandline arguments
const args: ICmdArgs = readAndValidateArgs();

const USER_FILE_PATH = args.usersFilepath;
const TWEETS_FILE_PATH = args.tweetsFilepath;

// Read file content of users and tweets
const usersStr = readFileSync(USER_FILE_PATH).toString("utf-8");
const tweetsStr = readFileSync(TWEETS_FILE_PATH).toString("utf-8");

// Instantiate repositories for interfacing with data
const userRepo = new UserRepository();
const tweetRepo = new TweetRepository();

// Populate repositories from data
userRepo.buildRepository(usersStr);
tweetRepo.buildRepository(tweetsStr);


// Get the user and tweet objects
const users = userRepo.users;
const tweets = tweetRepo.getTweets([])

// View Users and Tweets for debugging
// console.log(users)
// console.log(tweets)

// Print the feed
users.forEach(user => {
    console.log(user.name)

    tweets.forEach(tweet => {
        if(user.following.has(tweet.username)) {
            console.log(`@${tweet.username}: ${tweet.msg}`);
        }
    })
})