/**
 * Author: Andrew Cloete
 * Created: 3 July 2018
 */


import { User } from "./User";
import { Repository } from "./Repo"

export class UserRepository extends Repository {
    private _users: Map<string, User>;
    private LINEBREAK = "\r\n" // Assumes windows style line endings (\r\n)
    private SEPARATOR = " follows "

    constructor() {
        super();
        this._users = new Map()
    }

    public get(username: string): User {
        return this._users.get(username);
    }

    public remove(username: string) {
        // TODO
    }

    public has(username: string): boolean {
        return this._users.has(username);
    }

    public upsert(user: User) {
        this._users.set(user.name, user);
    }

    /**
     * Deserialises a 7-bit ASCII string to user objects
     * 
     * @param usersAscii The single raw string of user messages
     */
    public buildRepository(usersAscii: string) {

        const entries = this.parseAscii(usersAscii, this.LINEBREAK, this.SEPARATOR)
        
        // Populate users from entries
        entries.forEach(entry => {
            let user: User;
            try {
                user = new User(entry.key);

                // Parse followings and trim any whitespace
                const followings = entry.value.split(',').map(nameStr => {
                    return nameStr.trim();
                });

                // Add to internal map if no errors thrown
                if(!this._users.has(user.name)) {
                    this._users.set(user.name, user)
                }

                followings.forEach(following => {

                    // Create user for each following if none exists
                    if(!this._users.has(following)) {
                        const followingUser = new User(following);
                        this._users.set(followingUser.name, followingUser)
                    }

                    this._users.get(user.name).addFollowing(following)
                })
            }
            catch (e) {
                console.warn(e.message);
            }
        })
    }


    public get users(): User[] {
        return Array.from(this._users.values())
    }

}