/**
 * Author: Andrew Cloete
 * Created: 3 July 2018
 */

const minimist = require("minimist");
import { lstatSync } from "fs";


export interface ICmdArgs {
    usersFilepath: string;
    tweetsFilepath: string;
}


function validateFile(filepath) {
    try {
        if(!lstatSync(filepath).isFile()) {
            throw new Error(`${filepath} is not a file`)
        }
    }
    catch (e) {
        throw new Error(`Cannot read ${filepath}: ${e}. Try using absolute path`)
    }

}

export function readAndValidateArgs(): ICmdArgs {

    const args = minimist(process.argv.slice(2));

    if(!args["users"] && !args["u"]) {
        throw new Error("No input file for users specified");
    }
    
    if(!args["tweets"] && !args["t"]) {
        throw new Error("No input file for tweets specified");
    }

    
    const usersFilepath = args["users"] || args["u"];
    const tweetsFilepath = args["tweets"] || args["t"];
    validateFile(usersFilepath)
    validateFile(tweetsFilepath)

    
    return {
        usersFilepath: usersFilepath,
        tweetsFilepath: tweetsFilepath
    }

}





