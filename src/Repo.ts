/**
 * Author: Andrew Cloete
 * Created: 4 July 2018
 */


export interface IAsciiExportLine {
    key: string;
    value: string;
}


export class Repository {

    constructor() {}

    /**
     * Parses the assignment specific ASCII documents in to a key/value array
     * 
     * @param asciiStr: The input string
     * @param linebreak: The character(s) that separates the line (usually "\r" or "\r\n")
     * @param speparator: The character(s) that separates the key from value
     */
    protected parseAscii(asciiStr: string, linebreak: string, speparator): IAsciiExportLine[] {

        // Check string contains at least the needed semantic characters
        if(!asciiStr.includes(linebreak) || !asciiStr.includes(speparator)) {
            throw new Error("Invalid ASCII string. Cannot deserialise.")
        }

        const result: IAsciiExportLine[] = [];

        // Remove trailing linebreak if present
        if(asciiStr.endsWith(linebreak)) {
            asciiStr = asciiStr.substring(0, asciiStr.lastIndexOf(linebreak));
        }

        // Parse 
        const lines = asciiStr.split(linebreak);
        lines.forEach(line => {
            const substrs = line.split(speparator);
            result.push({
                key: substrs[0],
                value: substrs[1]
            });
        })

        return result
    }
}