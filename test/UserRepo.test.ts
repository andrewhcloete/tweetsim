/**
 * Author: Andrew Cloete
 * Created: 3 July 2018
 */

import { readFileSync } from "fs";
import { UserRepository } from "../src/UserRepo";

describe("User repository", () => {

    const FILE_PATH = "/home/andrew/Workspace/tweetSim/resources/user.txt"
    let userRepo: UserRepository;

    it("Builds a user repository", () => {
        userRepo = new UserRepository();
        const usersStr = readFileSync(FILE_PATH).toString("utf-8");
        userRepo.buildRepository(usersStr);
        expect(userRepo.users.length).toBe(3);

        const names = ['Alan', 'Martin', 'Ward']
        userRepo.users.forEach(user => {
            expect(names).toContain(user.name);
        });
    })

})