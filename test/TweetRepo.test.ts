/**
 * Author: Andrew Cloete
 * Created: 3 July 2018
 */


import { readFileSync } from "fs";
import { TweetRepository } from "../src/TweetRepo";

describe("Tweet repository", () => {

    const FILE_PATH = "/home/andrew/Workspace/tweetSim/resources/tweet.txt"
    let tweetRepo: TweetRepository;

    it("Confirms invalid tweets fails", () => {
        expect(() => {
            tweetRepo.buildRepository("foo......bar  baz")
        }).toThrow();
    })

    it("Builds a Tweet repository", () => {
        tweetRepo = new TweetRepository();
        const tweetsStr = readFileSync(FILE_PATH).toString("utf-8");
        tweetRepo.buildRepository(tweetsStr);
        expect(tweetRepo._tweets.length).toBe(3);

    })

    it("Gets tweets from the Tweets repo", () => {
        // All tweets
        const allTweets = tweetRepo.getTweets([]);
        expect(allTweets.length).toBe(3);

        // Alan Tweets
        const alanTweets = tweetRepo.getTweets(["Alan"]);
        expect(alanTweets.length).toBe(2);
        alanTweets.forEach(tweet => {
            expect(tweet.username).toBe("Alan")
        })

        // Ward tweets
        const wardTweets = tweetRepo.getTweets(["Ward"]);
        expect(wardTweets.length).toBe(1);
        wardTweets.forEach(tweet => {
            expect(tweet.username).toBe("Ward")
        })
        
        // Both tweets
        const bothTweets = tweetRepo.getTweets(["Alan", "Ward"]);
        expect(bothTweets.length).toBe(3);

        //None tweets
        const noneTweets = tweetRepo.getTweets(["Psg"]);
        expect(noneTweets.length).toBe(0);
    })

    it("Gets users from the Tweets repo", () => {
        const users = tweetRepo.getUsers();
        expect(users.length).toBe(2);
        expect(users).toContain("Alan")
        expect(users).toContain("Ward")
    });

})