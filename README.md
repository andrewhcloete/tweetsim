# TweetSim

Author: Andrew Cloete, andrewhcloete@gmail.com

## Purpose:
Simulates a twitter-like feed given two seven bit ACII text files

## Note to the examiner
With the codebase I try to convey good OO design that conforms the SOLID principles. At this point I am overloaded with commitments and will not be able complete this assignment to the point which I would deem it ready for production deployment:

Some things I would have liked to include before production:
- Install `path` module to reference ASCII file using a *relative* path
- Unit tests for all modules (not just `TweetRepo` and `UserRepo`)
- Unit tests that checks boundary conditions and exceptions
- Integration testing for the final output of the program
- Custom test data that includes corrupt data i.e not just testing using the example data
- Environment meta-info setup that makes specifics such as filepaths and delimiters configurable without modification of code.
- In-code documentation e.g. more elaborate comments on non-trival routines and file purpose descriptions


**Should you the examiner feel that it is "almost there" but you want to see some more in one specific area, feel free to contact me and prompt me for more without being too specific.**

## Getting started
- Install dependencies: `npm install`
- Test: `npm run test` (requires modification of `FILE_PATH` first in both test files)
- Build: `npm run build`
- Run: `node dist/main.js -u /absolute/path/to/user.txt -t /absolute/path/to/tweet.txt`


## Dependencies
- Node.js v.9 or higher
- NPM v.6 or higher
- (see `package.json` for list of NPM dependencies)